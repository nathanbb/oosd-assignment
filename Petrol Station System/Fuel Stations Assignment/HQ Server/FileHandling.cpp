#include "stdafx.h"

//-----------------------------------------------------------------------------
// Name: FileHandling()
// Desc: Empty constructor, initializes the class
//-----------------------------------------------------------------------------
FileHandling::FileHandling()
{

}

//-----------------------------------------------------------------------------
// Name: getInputStream(char* fileName, ifstream& fileIn)
// Desc: Opens the file input stream
//-----------------------------------------------------------------------------
bool FileHandling::getInputStream(char* fileName, ifstream& fileIn)
{
	// Open the file for reading
	fileIn.open( fileName );
	// If it failed to open, notify and return false
    if( fileIn.fail() ) {
      cout << "Failed to open input file: " << fileName << ". Does the file exist?" << endl;
      return false;
    } // Otherwise, notify and return true
    else {
      cout << "Input file: " << fileName << " open." << endl;
	  return true;
    }
}

//-----------------------------------------------------------------------------
// Name: getOutputStream(char* fileName, ofstream& fileOut)
// Desc: Opens the file output stream
//-----------------------------------------------------------------------------
bool FileHandling::getOutputStream(char* fileName, ofstream& fileOut)
{
	// Open the file for appending
	fileOut.open( fileName, ios::app );
	// If it failed to open, notify and return false
	if( fileOut.fail() ) {
		cout << "Failed to open output file: " << fileName << endl;
		return false;
	} // Otherwise, notify and return true
	else {
		cout << "Output file: " << fileName << " open." << endl;
		return true;
	}
}

//-----------------------------------------------------------------------------
// Name: closeStream(ifstream& stream)
// Desc: Closes the file input stream
//-----------------------------------------------------------------------------
void FileHandling::closeStream(ifstream& stream)
{
	// Close the stream
	stream.close();
}

//-----------------------------------------------------------------------------
// Name: closeStream(ofstream& stream)
// Desc: Closes the file output stream
//-----------------------------------------------------------------------------
void FileHandling::closeStream(ofstream& stream)
{
	// Close the stream
	stream.close();
}

//-----------------------------------------------------------------------------
// Name: readInput(ifstream& inFile, float& height, float& weight, bool& gender, string& name) 
// Desc: Reads a file from the disk
//-----------------------------------------------------------------------------
void FileHandling::readInput(ifstream& inFile, float& height, float& weight, bool& gender, string& name) 
{
	string toWeight = "";
	string toHeight = "";

	char currentChar;
	int line = 0;
	while (inFile.get(currentChar))
	{
		if (currentChar != '\n')
		{
			if (line == 0)
			{
				name += currentChar;
			}
			if (line == 1)
			{
				toWeight += currentChar;
			}
			if (line == 2)
			{
				toHeight += currentChar;
			}
			if (line == 3)
			{
				if (currentChar == '0')
					gender = false;
				else
					gender = true;
			}
		}
		else
			line++;
	}
	weight = (float)atof(toWeight.c_str());
	height = (float)atof(toHeight.c_str());
}

//-----------------------------------------------------------------------------
// Name: saveOutput(ofstream& outFile, string data)
// Desc: Writes data to the disk
//-----------------------------------------------------------------------------
void FileHandling::saveOutput(ofstream& outFile, string data)
{
	outFile << data << endl;
}