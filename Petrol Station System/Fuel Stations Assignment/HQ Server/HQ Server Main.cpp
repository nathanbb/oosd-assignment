#include "stdafx.h"
#include "winsock2.h"
#include "conio.h"
#include <string>


#define SERVER_PORT 12345
#define BUF_SIZE 256				// block transfer size  
#define QUEUE_SIZE 256 

const char* prefix = "";			// Prefix to add to the beginning of a file that is being saved to - used for adding directory
const char* suffix = ".txt";		// Suffix to add to the end of a file that is being saved to
mutex		filestreamMutex;		// Mutex used for controlling data output to disk
mutex		recievePacketMutex;
char		recvbuf[256];

void SplitInput(char* input, char*& output, char*& id);		// Splits up the input into the id of the station that sent it, and the required data contained in it
void SafeSaveToDisk(char recvbuf[BUF_SIZE]);
void SafeAcceptConnection(SOCKET&, SOCKET&, int&);

//-----------------------------------------------------------------------------
// Name: _tmain(int argc, _TCHAR* argv[])
// Desc: Main method for the application, run until the exit command is sent
//-----------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	int		b, l, on		= 1;
	SOCKET	s, sa;
	struct	sockaddr_in channel;	// holds IP address 
	WORD	wVersionRequested;
	WSADATA wsaData;
	int		err;
	int		bytesRecv;

	bool	bExit = false;			// Stores whether the exit command has been called
	
	// Give the console window a title
	SetConsoleTitle(L"HQ Server");
	
	//--- INITIALIZATION -----------------------------------
	wVersionRequested = MAKEWORD( 1, 1 );
	err = WSAStartup( wVersionRequested, &wsaData );

	if ( err != 0 ) {
		printf("WSAStartup error %ld", WSAGetLastError() );
		WSACleanup();
		return false;
	}
	//------------------------------------------------------

	  
	//---- Build address structure to bind to socket.--------  
	memset(&channel, 0, sizeof(channel));// zerochannel 
	channel.sin_family = AF_INET; 
	channel.sin_addr.s_addr = htonl(INADDR_ANY); 
	channel.sin_port = htons(SERVER_PORT); 
	//--------------------------------------------------------


	// ---- create SOCKET--------------------------------------
	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);    
	if (s < 0) {
		printf("socket error %ld",WSAGetLastError() );
		WSACleanup();
		return false;
	}

	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on)); 
	//---------------------------------------------------------

	//---- BIND socket ----------------------------------------
	b = ::bind(s, (struct sockaddr *) &channel, sizeof(channel)); 
	if (b < 0) {
		printf("bind error %ld", WSAGetLastError() ); 
		WSACleanup();
		return false;
	}
	//----------------------------------------------------------

	//---- LISTEN socket ----------------------------------------
	l = listen(s, QUEUE_SIZE);                 // specify queue size 
	if (l < 0) {
		printf("listen error %ld",WSAGetLastError() );
		WSACleanup();
		return false;
	}
	//-----------------------------------------------------------

	// While the exit command hasn't been recieved, wait for input
	while (!bExit) 
	{
		SafeAcceptConnection(sa, s, err);

		//SafeSaveToDisk(recvbuf);
		
		//----------------
	}
	
	closesocket( s );
	WSACleanup();
	return 0;
}

void SafeAcceptConnection(SOCKET& sa, SOCKET& s, int& err)
{
	lock_guard<mutex> mGuard(recievePacketMutex);
	//---- ACCEPT connection ------------------------------------
	sa = accept(s, 0, 0);                  // block for connection request  
	if (sa < 0) {
		printf("accept error %ld ", WSAGetLastError() ); 
		WSACleanup();
	}
	else {
		printf("connection accepted");
	}
	//------------------------------------------------------------
	// Socket is now set up and bound. Wait for connection and process it. 
	
	//---- RECV bytes --------------------------------------------
	int bytesRecv = recv( sa, recvbuf, 100, 0 );
	err = WSAGetLastError( );// 10057 = A request to send or receive data was disallowed because the socket is not connected and (when sending on a datagram socket using a sendto call) 
	if ( bytesRecv == 0 || bytesRecv == WSAECONNRESET ) {
		printf( "Connection Closed.\n");
		WSACleanup();
	}
	printf( " Bytes Recv: %s \n ", recvbuf );
	closesocket( sa );
	//-------------------------------------------------------------
	
	SafeSaveToDisk(recvbuf);
}

void SafeSaveToDisk(char data[256])
{
	lock_guard<mutex> mGuard(filestreamMutex);
	// Save data to file
	FileHandling diskFile;
	char fullPath[_MAX_PATH];
	ofstream fout;
	const char* fileName;
	char* id = "";
	char* output = "";
	SplitInput(data, output, id);

	fileName = id;
	strcpy_s(fullPath, _MAX_PATH, prefix);
	strcpy_s(fullPath, _MAX_PATH, "Station ");
	strcat_s(fullPath, _MAX_PATH, fileName);
	strcat_s(fullPath, _MAX_PATH, suffix);

	diskFile.getOutputStream(fullPath, fout);
	diskFile.saveOutput(fout, output);
	diskFile.closeStream(fout);
	cout << "Saved to disk." << endl;
}

//-----------------------------------------------------------------------------
// Name: SplitInput(char* input, char*& output, char*& id)
// Desc: Initializes the various fuel pumps
//-----------------------------------------------------------------------------
void SplitInput(char* input, char*& output, char*& id)
{
	// Get current system time to add to beginning of data to save to file
	time_t timeNow = time(0);
	char* outChar = ctime(&timeNow);
	struct tm* datetime = localtime(&timeNow);
	// Output the date in the ISO 8601 format YYYY/MM/DD, and time in HH:MM:SS, with a hyphen to separate it from the log data
	strftime(outChar, 1024, "%Y/%m/%d %H:%M:%S -", datetime);

	string strInput, strID, strOutput;
	strInput = input;
	// Find the identifier character `
	unsigned pos = strInput.find("`");
	// Station ID will be from index 0 to the identifier characters position
	strID = strInput.substr(0, pos);
	// Data will be from one more than the identifier charaters position to the end of the string
	strOutput = strInput.substr(pos+1);

	// cast the substrings to char*
	char* outputChar	= _strdup(strOutput.c_str());
	char* idChar		= _strdup(strID.c_str());

	// add cast output data to the output char
	strcat_s(outChar, 1024, outputChar);

	// returns the required data
	output = outChar;
	id = idChar;
}