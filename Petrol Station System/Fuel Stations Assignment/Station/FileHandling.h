#pragma once
#include <fstream>	// fstream, used for accessing local disk files
#include <string>	// string, used for storing strings, more versatile than char arrays

using namespace std;

//-----------------------------------------------------------------------------
// Name: FileHandling
// Desc: Class used to read and write data to/from the file system
//-----------------------------------------------------------------------------
class FileHandling
{
public:
	FileHandling();													// Empty constructor, initializes the class

	bool getInputStream(char* fileName, ifstream& fileIn);			// Opens the file input stream
	bool getOutputStream(char* fileName, ofstream& fileOut);		// Opens the file output stream
	void closeStream(ifstream&);									// Closes the file input stream
	void closeStream(ofstream&);									// Closes the file output stream
	void readInput(ifstream &, float&, float&, bool&, string&);		// Reads a file from the disk
	void saveOutput(ofstream &, string);							// Writes data to the disk
};