#pragma once
#include "Vehicle.h"

//-----------------------------------------------------------------------------
// Name:	FuelType
// Desc:	Enum used to set which fuel type is used
// Options:	Petrol
//			Diesel
//			Electric
//-----------------------------------------------------------------------------
enum FuelType
{
	Petrol,
	Diesel,
	Electric
};

//-----------------------------------------------------------------------------
// Name: FuelPump
// Desc: Class used to store information about a fuel pump used by
// the station.
//-----------------------------------------------------------------------------
class FuelPump
{
public:
	FuelPump() { }	// Empty constructor, initializes the class
	FuelPump(string id, FuelType pType, double cost)	{ pumpID = id;	type = pType; price = cost; }	// Constructor, initializes the class
	~FuelPump();					// Destructor, frees memory used by the class

	void		SetPrice(double);	// Set the price the fuel will cost
	double		GetPrice()			{ return price;		}		// Returns the price of the fuel
	string		GetID()				{ return pumpID;	}		// Returns the ID of the pump
	bool		InUse()				{ return used;		}		// Returns whether the pump is in use
	void		Use()				{ used = true;		}		// Sets the pump to in use
	void		Finished()			{ used = false;		}		// Sets the pump as not in use
	FuelType	GetType()			{ return type;		}		// Returns the fuel type of the pump
	int			TimeWaited()		{ return waitTime;	}		// Returns the time waited
	void		WaitLonger()		{ waitTime++;		}		// Increments the wait time
	void		WaitOver()			{ waitTime = 0;		}		// Reset wait time
	
private:
	FuelType	type;				// Stores the type of fuel used by the pump
	double		price;				// Stores the price of the fuel
	string		pumpID;				// Stores the ID of the pump
	bool		used;				// Stores whether the pump is in use
	int			waitTime;			// Stores the time to wait before vehicle is done
};

//-----------------------------------------------------------------------------
// Name: PetrolDieselPump
// Desc: Subclass used to store information about a petrol pump used by
// the station.
//-----------------------------------------------------------------------------
class PetrolDieselPump : public FuelPump
{
public:
	PetrolDieselPump() : FuelPump("", Petrol, 0) { };			// Empty constructor, initializes the class
	PetrolDieselPump(string id, FuelType pType, double cost)
		: FuelPump(id, pType, cost) { };						// Constructor, initializes the class
	~PetrolDieselPump();										// Destructor, frees memory used by the class

	void	OccupiedBy(PetrolDieselVehicle v) {vehicle = v;}	// Adds a vehicle to occupy the pump
	void	Empty() { vehicle.~PetrolDieselVehicle();}			// Discard vehicle object

	float	GetRequiredFuel()	{return vehicle.GetRequiredFuel();}	// Returns the fuel required
	string	GetNumPlate()		{return vehicle.GetNumberPlate();}	// Returns the vehicles num plate
	bool	IsLoyal()			{return vehicle.Loyal();}			// Returns if the owner has a loyalty card

private:

	PetrolDieselVehicle vehicle;	// Vehicle occupying the pump
};

//-----------------------------------------------------------------------------
// Name: ElectricChargingStation
// Desc: Subclass used to store information about a petrol pump used by
// the station.
//-----------------------------------------------------------------------------
class ElectricChargingStation : public FuelPump
{
public:
	ElectricChargingStation() : FuelPump("", Petrol, 0) { };	// Empty constructor, initializes the class
	ElectricChargingStation(string id, FuelType pType, double cost)
		: FuelPump(id, pType, cost) { };						// Constructor, initializes the class
	~ElectricChargingStation();									// Destructor, frees memory used by the class

	void	OccupiedBy(ElectricVehicle v) {vehicle = v;}		// Adds a vehicle to occupy the charging station
	void	Empty() { vehicle.~ElectricVehicle();}				// Discard vehicle object
	
	int		GetRequiredFuel()	{return vehicle.GetRequiredFuel();}	// Returns the fuel required
	string	GetNumPlate()		{return vehicle.GetNumberPlate();}	// Returns the vehicles num plate
	bool	IsLoyal()			{return vehicle.Loyal();}			// Returns if the owner has a loyalty card

private:

	ElectricVehicle vehicle;		// Vehicle occupying the charging station
};