//-----------------------------------------------------------------------------
// Name:			Station Main
// Desc:			Main update logic for fuel stations.
//
// About Project:	Created for Object-Oriented Systems Development Assignment,
// this programme is a server-client system that simulates a chain of
// fuel stations that serve petrol, diesel, and electric vehicles.
// the client records each vehicle that passes through the station and
// reports each transaction to HQ.
//
// Author Name:			Nathan Boxhall-Burnett
// Author Institution:	University of Huddersfield
// Author Student ID:	U1254544
//-----------------------------------------------------------------------------

#include "stdafx.h"		// precompiled header
#include <iostream>		// iostream, improved alternative to conio
#include <string>		// string, used for storing strings, more versatile than char arrays
#include <queue>		// Queue, used for the basic queueing system for the vehicles
#include "winsock2.h"	// winsock2, used for accessing the server client
#include "conio.h"		
#include "FuelPump.h"	// FuelPump, stores the info on individual fuel pumps
#include "Vehicle.h"	// Vehicle, stores the info on the vehicles using the station
using namespace std;

#define SERVER_PORT		12345					// Port the client will use to connect to the server
#define BUF_SIZE		256						// Block transfer size  
#define IPAddress		"127.0.0.1"				// IP Address of the server, 127.0.0.1 to loop back to local machine

WORD		wVersionRequested;
WSADATA		wsaData;
SOCKADDR_IN target;								//Socket address information
SOCKET		s;
int			err;
int			bytesSent;
char		stationID[BUF_SIZE] = "";			// Stores the ID of the fuel station
bool		bExit				= false;		// Stores whether the exit command has been called
bool		bViewCustomers		= false;		// Stores whether to display station use in the console
mutex		coutMutex;							// Mutex used for controlling usage of cout
mutex		sendDataMutex;						// Mutex used for controlling sending data to HQ
mutex		petrolQueueMutex;					// Mutex used for controlling the access to the petrol queue
mutex		dieselQueueMutex;					// Mutex used for controlling the access to the diesel queue
mutex		electricQueueMutex;					// Mutex used for controlling the access to the electric queue

// Fuel pump variables
PetrolDieselPump		petrolPumps[6];			// Stores the instances of the petrol fuel pump FuelPump class
PetrolDieselPump		dieselPumps[6];			// Stores the instances of the diesel fuel pump FuelPump class
ElectricChargingStation	electricPumps[6];		// Stores the instances of the electric charging stations FuelPump class
int			pPumpCount			= -1;			// Stores the number of petrol pumps
int			dPumpCount			= -1;			// Stores the number of diesel pumps
int			ePumpCount			= -1;			// Stores the number of electric charging stations
string		petrolPrefix		= "petrol ";	// Prefix for the naming of the petrol fuel pumps
string		dieselPrefix		= "diesel ";	// Prefix for the naming of the diesel fuel pumps
string		electricPrefix		= "electric ";	// Prefix for the naming of the electric charging stations
int			petrolTimeWaited	= 0;			// Stores the time waited between petrol vehicles arriving
int			dieselTimeWaited	= 0;			// Stores the time waited between diesel vehicles arriving
int			electricTimeWaited	= 0;			// Stores the time waited between electric vehicles arriving
int			petrolTimeToWait	= 0;			// Stores the time to wait for the petrol update
int			dieselTimeToWait	= 0;			// Stores the time to wait for the diesel update
int			electricTimeToWait	= 0;			// Stores the time to wait for the electric update
queue<PetrolDieselVehicle*>		petrolQueue;	// Used to store the queue for the petrol pumps 
queue<PetrolDieselVehicle*>		dieselQueue;	// Used to store the queue for the diesel pumps
queue<ElectricVehicle*>			electricQueue;	// Used to store the queue for the electric charging stations
default_random_engine			random;			// random number engine
uniform_int_distribution<int>	wait(2000, 20000);// random number generator used for time between vehicle arrivals


// Forward Declarations
bool	SendData(char data[BUF_SIZE]);			// Sends data to the HQ server
void	InitializePumps();						// Initializes the various fuel pumps
void	ManualInput();							// Used to manually input commands to the system	
void	RunStation();							// Updates the vehicle/pump system
void	RunStation(bool seperateThread);		// Updates the vehicle/pump ststem independently from the main thread
void	UpdatePetrol();							// Updates the petrol pump system
void	UpdateDiesel();							// Updates the diesel pump system
void	UpdateElectric();						// Updates the electric charging system

void	SafeCOUT(string msg, bool, bool);			// Threading safe usage of the shared method cout
void	SafePetrolQueuePush(PetrolDieselVehicle*);	//Threading safe usage of the petrol pumps queue
void	SafePetrolQueuePop();						//Threading safe usage of the petrol pumps queue
void	SafeDieselQueuePush(PetrolDieselVehicle*);	//Threading safe usage of the diesel pumps queue
void	SafeDieselQueuePop();						//Threading safe usage of the diesel pumps queue
void	SafeElectricQueuePush(ElectricVehicle*);	//Threading safe usage of the electric charging stations queue
void	SafeElectricQueuePop();						//Threading safe usage of the electric charging stations queue


//-----------------------------------------------------------------------------
// Name: _tmain(int argc, _TCHAR* argv[])
// Desc: Main method for the application, run until the exit command is sent
//-----------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	// Give the console window a title
	SetConsoleTitle(L"Petrol Station Client");

	SafeCOUT("Please enter ID name of station: ", false, false);
	cin >> stationID;
	
	// Set console title to include station ID
	string title = "Petrol Station Client " + (string)stationID;
	wstring wTitle = wstring(title.begin(), title.end());
	SetConsoleTitle(wTitle.c_str());

	InitializePumps();

	//Make sure the queues are empty
	for (int i = 0; i < petrolQueue.size(); i++)
		petrolQueue.pop();
	for (int i = 0; i < dieselQueue.size(); i++)
		dieselQueue.pop();
	for (int i = 0; i < electricQueue.size(); i++)
		electricQueue.pop();

	unsigned maxThreads = thread::hardware_concurrency();

	// If there is more than one possible thread, create the station logic in a detached thread to run by itself.
	if (maxThreads > 1)
	{
		// Compiler doesn't understand what is going on for an overloaded method, so using lambda closure [=] to capture the referenced method
		thread stationThread([=]{RunStation(true);});	// Create thread
		stationThread.detach();							// Detatch thread
	}

	// While the exit command hasn't been sent, wait for input
	while(!bExit) {
		// If view customers has been called, don't check for any more manual input
		if (!bViewCustomers)
			ManualInput();

		// If there are not multiple threads available, update station
		if (maxThreads < 2)
			RunStation();
	}

	return 0;
}

//-----------------------------------------------------------------------------
// Name: InitializePumps()
// Desc: Initializes the various fuel pumps
//-----------------------------------------------------------------------------
void InitializePumps()
{
	string idNum = "";

	// Create the petrol pumps
	do
	{
		/*SafeCOUT("Enter the number of Petrol Pumps [0 - 6]: ", false, false);
		cin >> pPumpCount;
		SafeCOUT("", true, false);*/
		pPumpCount = 6;
	}
	while (pPumpCount != 0
		&& pPumpCount != 1
		&& pPumpCount != 2
		&& pPumpCount != 3
		&& pPumpCount != 4
		&& pPumpCount != 5
		&& pPumpCount != 6);

	if (pPumpCount != 0)
	{
		for (int i = 0; i < pPumpCount; i++)
		{
			string id = to_string(i);
			string pumpID = petrolPrefix + "#" + id;
			
			petrolPumps[i] = PetrolDieselPump(pumpID, Petrol, 1.29);
		}
	}
	
	// Create the diesel pumps
	do
	{
		/*SafeCOUT("Enter the number of Diesel Pumps: [0 - 6]: ", false, false);
		cin >> dPumpCount;
		SafeCOUT("", true, false);*/
		dPumpCount = 6;
	}
	while (dPumpCount != 0
		&& dPumpCount != 1
		&& dPumpCount != 2
		&& dPumpCount != 3
		&& dPumpCount != 4
		&& dPumpCount != 5
		&& dPumpCount != 6);

	if (dPumpCount != 0)
	{
		for (int i = 0; i < dPumpCount; i++)
		{
			string id = to_string(i);
			string pumpID = dieselPrefix + "#" + id;
			
			dieselPumps[i] = PetrolDieselPump(pumpID, Diesel, 1.36);
		}
	}

	// Create the electric charging points
	do
	{
		/*SafeCOUT("Enter the number of Electric Charging Points [0 - 6]: ", false, false);
		cin >> ePumpCount;
		SafeCOUT("", true, false);*/
		ePumpCount = 6;
	}
	while (ePumpCount != 0
		&& ePumpCount != 1
		&& ePumpCount != 2
		&& ePumpCount != 3
		&& ePumpCount != 4
		&& ePumpCount != 5
		&& ePumpCount != 6);

	if (ePumpCount != 0)
	{
		for (int i = 0; i < ePumpCount; i++)
		{
			string id = to_string(i);
			string pumpID = electricPrefix + "#" + id;
			
			electricPumps[i] = ElectricChargingStation(pumpID, Electric, 0.70);
		}
	}
}

//-----------------------------------------------------------------------------
// Name: ManualInput()
// Desc: Used to manually input commands to the system	
//-----------------------------------------------------------------------------
void ManualInput()
{
	string command;

	system ("cls");
	SafeCOUT("Welcome to OOSD assignment fuel station " + (string)stationID + ".\n\n"
		"Vehicle statistics will automatically be sent to HQ,\n"
		"but for any statistical enquiries, please enter one of the\n"
		"following commands below:\n\n"
		"'Exit' to exit the programme\n"
		"'Send' to Send a message to HQ\n"
		"'ViewCustomers' to view station progress\n\n"
		"Command: ", false, false);
	cin >> command;

	//Ugly if checks, C++ doesn't like switch statements for strings
	if (command != "Send" 
		&& command != "Exit" 
		&& command != "ViewCustomers")
	{
		while (command != "Send" 
			&& command != "Exit" 
			&& command != "ViewCustomers")
		{
			SafeCOUT("Unknown command, please enter either 'Send', 'ViewCustomers' or 'Exit' to proceed.", true, false);
			SafeCOUT("Command: ", false, false);
			cin >> command;
		}
	}
	else if (command == "Send")
	{
		// Get keyboard input for data to be sent to the server
		char input[256] = "";
		char output[256] = "";
		SafeCOUT("Enter message to send to HQ: ", false, false);
		cin >> input;

		// concatenate output to include the ID of the station sending the info, and the splitting ID ` 
		strcpy_s(output, BUF_SIZE, stationID);
		strcat_s(output, BUF_SIZE, "`");
		strcat_s(output, BUF_SIZE, input);
		SendData(output);
	}
	else if (command == "ViewCustomers")
	{
		// Set bViewCustomers to true and clear the console
		bViewCustomers = true;
		system ("cls");
	}
	else if(command == "Exit")
	{
		bExit = true;
	}
}

//-----------------------------------------------------------------------------
// Name: SendData(char data[BUF_SIZE])
// Desc: Sends data to the HQ server
//-----------------------------------------------------------------------------
bool SendData(char data[BUF_SIZE])
{
	// RAII mechanism for the mutex. While this method is in focus, lock the mutex, allowing only one instance
	lock_guard<mutex> mGuard(sendDataMutex);

	//--- INITIALIZATION --------------------------------------
	wVersionRequested = MAKEWORD( 1, 1 );
	err = WSAStartup( wVersionRequested, &wsaData );

	if ( err != 0 ) {
		printf("WSAStartup error %ld", WSAGetLastError() );
		WSACleanup();
		return false;
	}
	//---------------------------------------------------------
	
	//---- Build address structure to bind to socket.----------
	target.sin_family = AF_INET; // address family Internet
	target.sin_port = htons (SERVER_PORT); //Port to connect on
	target.sin_addr.s_addr = inet_addr (IPAddress); //Target IP
	//---------------------------------------------------------

	
	// ---- create SOCKET--------------------------------------
	s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); //Create socket
	if (s == INVALID_SOCKET)
	{
		printf("socket error %ld" , WSAGetLastError() );
		WSACleanup();
		return false; //Couldn't create the socket
	}  
	//----------------------------------------------------------

	
	//---- try CONNECT -----------------------------------------
	if (connect(s, (SOCKADDR *)&target, sizeof(target)) == SOCKET_ERROR)
	{
		printf("connect error %ld", WSAGetLastError() );
		WSACleanup();
		return false; //Couldn't connect
	}
	//-----------------------------------------------------------
	
	//---- SEND bytes -------------------------------------------
	bytesSent = send( s, data, BUF_SIZE, 0 ); 
	//printf( "Bytes Sent to HQ: %ld \n", bytesSent );
	//-----------------------------------------------------------
		
	closesocket( s );
	WSACleanup();

	// Sleep the thread for a second before the chance of sending another message
	this_thread::sleep_for(chrono::seconds(1));

	return true;
}

//-----------------------------------------------------------------------------
// Name: RunStation()
// Desc: Updates the vehicle/pump system
//-----------------------------------------------------------------------------
void RunStation()
{
	// If the hardware supports multithreading, update each pump set concurrently
	if (thread::hardware_concurrency() > 1)
	{
		thread petrolThread(UpdatePetrol);
		thread dieselThread(UpdateDiesel);
		thread electricThread(UpdateElectric);

		petrolThread.join();
		dieselThread.join();
		electricThread.join();
	}// Otherwise, update them separately
	else
	{
		UpdatePetrol();
		UpdateDiesel();
		UpdateElectric();
	}
}

//-----------------------------------------------------------------------------
// Name: RunStation(bool seperateThread)
// Desc: Updates the vehicle/pump system independently of the main thread 
//-----------------------------------------------------------------------------
void RunStation(bool seperateThread)
{
	// While the exit command hasn't been sent, update the station
	while (!bExit)
	{
		RunStation();
	}
}

//-----------------------------------------------------------------------------
// Name: UpdatePetrol()
// Desc: Updates the petrol pump system
//-----------------------------------------------------------------------------
void UpdatePetrol()
{
	if (pPumpCount > 0)
	{
		if (petrolTimeToWait == 0)
			petrolTimeToWait = wait(random);

		if (petrolTimeToWait > petrolTimeWaited)
			petrolTimeWaited++;
		else
		{
			petrolTimeToWait = 0;
			petrolTimeWaited = 0;
			SafeCOUT("----------------------------------------------------------------\n"
				"Queue Size: " + to_string(petrolQueue.size()) + "\nPetrol Car Arrived. ", true, true);
			
			uniform_int_distribution<int>	idGen(0, 1000), boolGen(0, 1);
			uniform_real_distribution<float> fuelGen(10, 70);
			string id = to_string(idGen(random));
			// Add vehicle to the queue
			PetrolDieselVehicle test = PetrolDieselVehicle(id, (bool)(boolGen(random)), fuelGen(random));
			SafePetrolQueuePush(&test);
		}

		// Check if there are free pumps and occupy if there are vehicles waiting
		for (int i = 0; i < pPumpCount; i++)
		{
			if (!petrolPumps[i].InUse())
			{
				if (petrolQueue.size() > 0)
				{
					petrolPumps[i].OccupiedBy(*petrolQueue.front());
					petrolPumps[i].Use();
					SafePetrolQueuePop();
				}
			}
		}

		// Service each active pump
		for (int i = 0; i < pPumpCount; i++)
		{
			if (petrolPumps[i].InUse())
			{
				int waitTime = petrolPumps[i].GetRequiredFuel() / 10;

				if (petrolPumps[i].TimeWaited() >= waitTime * 1000)
				{
					float cost = petrolPumps[i].GetRequiredFuel() * petrolPumps[i].GetPrice();
					if (petrolPumps[i].IsLoyal())
						cost *= 0.9;
				
					char costText[100];
					sprintf(costText, "�%1.2f", cost);

					string id = petrolPumps[i].GetNumPlate();

					string isLoyal = "";
					if (petrolPumps[i].IsLoyal())
						isLoyal = "Yes";
					else
						isLoyal = "No";

					string output = "Type:Petrol -Reg:" + petrolPumps[i].GetNumPlate() + " -HasCard:" + isLoyal + " -Spent:" + (string)costText;

					if (cost > 0)
					{
						SafeCOUT("----------------------------------------------------------------\n" + output, true, true);

						char outData[BUF_SIZE];
						strncpy(outData, output.c_str(), BUF_SIZE);

						char output[BUF_SIZE] = "";
						strcpy_s(output, BUF_SIZE, stationID);
						strcat_s(output, BUF_SIZE, "`");
						strcat_s(output, BUF_SIZE, outData);

						SendData(output);
					}
					petrolPumps[i].Finished();
					petrolPumps[i].Empty();
					petrolPumps[i].WaitOver();
				}
				else
					petrolPumps[i].WaitLonger();
			}
		}

	}
}

//-----------------------------------------------------------------------------
// Name: UpdatePetrol()
// Desc: Updates the diesel pump system
//-----------------------------------------------------------------------------
void UpdateDiesel()
{
	if (dPumpCount > 0)
	{
		if (dieselTimeToWait == 0)
			dieselTimeToWait = wait(random);

		if (dieselTimeToWait > dieselTimeWaited)
			dieselTimeWaited++;
		else
		{
			dieselTimeToWait = 0;
			dieselTimeWaited = 0;
			SafeCOUT("----------------------------------------------------------------\n"
				"Queue Size: " + to_string(dieselQueue.size()) + "\nDiesel Car Arrived. ", true, true);
			
			uniform_int_distribution<int>	idGen(0, 1000), boolGen(0, 1);
			uniform_real_distribution<float> fuelGen(10, 70);
			string id = to_string(idGen(random));
			// Add vehicle to the queue
			PetrolDieselVehicle test = PetrolDieselVehicle(id, (bool)(boolGen(random)), fuelGen(random));
			SafeDieselQueuePush(&test);
		}

		// Check if there are free pumps and occupy if there are vehicles waiting
		for (int i = 0; i < dPumpCount; i++)
		{
			if (!dieselPumps[i].InUse())
			{
				if (dieselQueue.size() > 0)
				{
					dieselPumps[i].OccupiedBy(*dieselQueue.front());
					dieselPumps[i].Use();
					SafeDieselQueuePop();
				}
			}
		}

		// Service each active pump
		for (int i = 0; i < dPumpCount; i++)
		{
			if (dieselPumps[i].InUse())
			{
				int waitTime = dieselPumps[i].GetRequiredFuel() / 10;

				if (dieselPumps[i].TimeWaited() >= waitTime * 1000)
				{
					float cost = dieselPumps[i].GetRequiredFuel() * dieselPumps[i].GetPrice();
					if (dieselPumps[i].IsLoyal())
						cost *= 0.9;
				
					char costText[100];
					sprintf(costText, "�%1.2f", cost);

					string id = dieselPumps[i].GetNumPlate();

					string isLoyal = "";
					if (dieselPumps[i].IsLoyal())
						isLoyal = "Yes";
					else
						isLoyal = "No";

					string output = "Type:Diesel -Reg:" + dieselPumps[i].GetNumPlate() + " -HasCard:" + isLoyal + " -Spent:" + (string)costText;

					if (cost > 0)
					{
						SafeCOUT("----------------------------------------------------------------\n" + output, true, true);

						char outData[BUF_SIZE];
						strncpy(outData, output.c_str(), BUF_SIZE);

						char output[BUF_SIZE] = "";
						strcpy_s(output, BUF_SIZE, stationID);
						strcat_s(output, BUF_SIZE, "`");
						strcat_s(output, BUF_SIZE, outData);

						SendData(output);
					}
					dieselPumps[i].Finished();
					dieselPumps[i].Empty();
					dieselPumps[i].WaitOver();
				}
				else
					dieselPumps[i].WaitLonger();
			}
		}

	}
}

//-----------------------------------------------------------------------------
// Name: UpdatePetrol()
// Desc: Updates the electric charging station sytem
//-----------------------------------------------------------------------------
void UpdateElectric()
{
	if (ePumpCount > 0)
	{
		if (electricTimeToWait == 0)
			electricTimeToWait = wait(random);

		if (electricTimeToWait > electricTimeWaited)
			electricTimeWaited++;
		else
		{
			electricTimeToWait = 0;
			electricTimeWaited = 0;
			SafeCOUT("----------------------------------------------------------------\n"
				"Queue Size: " + to_string(electricQueue.size()) + "\nElectric Car Arrived. ", true, true);
			
			uniform_int_distribution<int>	idGen(0, 1000), boolGen(0, 1);
			uniform_real_distribution<float> fuelGen(10, 90);
			string id = to_string(idGen(random));
			// Add vehicle to the queue
			ElectricVehicle test = ElectricVehicle(id, (bool)(boolGen(random)), fuelGen(random));
			SafeElectricQueuePush(&test);
		}

		// Check if there are free pumps and occupy if there are vehicles waiting
		for (int i = 0; i < ePumpCount; i++)
		{
			if (!electricPumps[i].InUse())
			{
				if (electricQueue.size() > 0)
				{
					electricPumps[i].OccupiedBy(*electricQueue.front());
					electricPumps[i].Use();
					SafeElectricQueuePop();
				}
			}
		}

		// Service each active pump
		for (int i = 0; i < ePumpCount; i++)
		{
			if (electricPumps[i].InUse())
			{
				int waitTime = electricPumps[i].GetRequiredFuel() / 10;

				// Electric battery info stored as int, so have to manually check this for initialization
				if (electricPumps[i].TimeWaited () < 0
					&& waitTime < 0)
				{
					electricPumps[i].Finished();
					electricPumps[i].Empty();
					electricPumps[i].WaitOver();
				}

				if (electricPumps[i].TimeWaited() >= waitTime * 1000)
				{
					float cost = electricPumps[i].GetRequiredFuel() * electricPumps[i].GetPrice();
					if (electricPumps[i].IsLoyal())
						cost *= 0.9;
				
					char costText[100];
					sprintf(costText, "�%1.2f", cost);

					string id = electricPumps[i].GetNumPlate();

					string isLoyal = "";
					if (electricPumps[i].IsLoyal())
						isLoyal = "Yes";
					else
						isLoyal = "No";

					string output = "Type:Electric -Reg:" + electricPumps[i].GetNumPlate() + " -HasCard:" + isLoyal + " -Spent:" + (string)costText;

					if (cost > 0)
					{
						SafeCOUT("----------------------------------------------------------------\n" + output, true, true);

						char outData[BUF_SIZE];
						strncpy(outData, output.c_str(), BUF_SIZE);

						char output[BUF_SIZE] = "";
						strcpy_s(output, BUF_SIZE, stationID);
						strcat_s(output, BUF_SIZE, "`");
						strcat_s(output, BUF_SIZE, outData);

						SendData(output);
					}
					electricPumps[i].Finished();
					electricPumps[i].Empty();
					electricPumps[i].WaitOver();
				}
				else
					electricPumps[i].WaitLonger();
			}
		}

	}
}

//-----------------------------------------------------------------------------
// Name: safeCOUT(string msg, bool endline, bool useViewCustomers)
// Desc: Threading safe usage of the shared method cout
//-----------------------------------------------------------------------------
void SafeCOUT(string msg, bool endline, bool useViewCustomers)
{
	// RAII mechanism for the mutex. While this method is in focus, lock the mutex, allowing only one instance
	lock_guard<mutex> mGuard(coutMutex);

	// If not asked to use useViewCustomers, send the message
	if (!useViewCustomers)
	{
		if (endline)
			cout << msg << endl;
		else
			cout << msg;
	}

	// If asked to use useViewCustomers, and bViewCustomers is true, send the message
	if (useViewCustomers && bViewCustomers)
	{
		if (endline)
			cout << msg << endl;
		else
			cout << msg;
	}
}

//-----------------------------------------------------------------------------
// Name: SafePetrolQueuePush(PetrolDieselVehicle *v)
// Desc: Threading safe usage of the petrol pumps queue
//-----------------------------------------------------------------------------
void SafePetrolQueuePush(PetrolDieselVehicle *v)
{
	lock_guard<mutex> mGuard(petrolQueueMutex);

	petrolQueue.push(&*v);
}

//-----------------------------------------------------------------------------
// Name: SafePetrolQueuePop()
// Desc: Threading safe usage of the petrol pumps queue
//-----------------------------------------------------------------------------
void SafePetrolQueuePop()
{
	lock_guard<mutex> mGuard(petrolQueueMutex);

	petrolQueue.pop();
}

//-----------------------------------------------------------------------------
// Name: SafeDieselQueuePush(PetrolDieselVehicle *v)
// Desc: Threading safe usage of the diesel pumps queue
//-----------------------------------------------------------------------------
void SafeDieselQueuePush(PetrolDieselVehicle *v)
{
	lock_guard<mutex> mGuard(dieselQueueMutex);
	
	dieselQueue.push(&*v);
}

//-----------------------------------------------------------------------------
// Name: SafeDieselQueuePop()
// Desc: Threading safe usage of the diesel pumps queue
//-----------------------------------------------------------------------------
void SafeDieselQueuePop()
{
	lock_guard<mutex> mGuard(dieselQueueMutex);

	dieselQueue.pop();
}

//-----------------------------------------------------------------------------
// Name: SafeElectricQueuePush(ElectricVehicle *v)
// Desc: Threading safe usage of the electric charging stations queue
//-----------------------------------------------------------------------------
void SafeElectricQueuePush(ElectricVehicle *v)
{
	lock_guard<mutex> mGuard(electricQueueMutex);
	
	electricQueue.push(&*v);
}

//-----------------------------------------------------------------------------
// Name: SafeElectricQueuePop()
// Desc: Threading safe usage of the electric charging stations queue
//-----------------------------------------------------------------------------
void SafeElectricQueuePop()
{
	lock_guard<mutex> mGuard(electricQueueMutex);

	electricQueue.pop();
}