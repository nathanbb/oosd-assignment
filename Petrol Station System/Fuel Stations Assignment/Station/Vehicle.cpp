#include "stdafx.h"
#include "Vehicle.h"

//-----------------------------------------------------------------------------
// Name: Vehicle(string id, FuelType fType, bool loyal)
// Desc: Constructor, initializes the class
//-----------------------------------------------------------------------------
Vehicle::Vehicle(string id, bool loyal)
{ 
	numberPlate		= id; 
	hasLoyaltyCard	= loyal;
}

//-----------------------------------------------------------------------------
// Name: ~Vehicle()
// Desc: Destructor, frees memory used by the class
//-----------------------------------------------------------------------------
Vehicle::~Vehicle()
{
}

PetrolDieselVehicle::PetrolDieselVehicle(string id, bool loyal, float fuel) : Vehicle(id, loyal)
{
	fuelRequired = fuel;
}

PetrolDieselVehicle::~PetrolDieselVehicle()
{
	
}

ElectricVehicle::ElectricVehicle(string id, bool loyal, int charge) : Vehicle(id, loyal) 
{ 
	batteryPercentageRequired = charge;
}

ElectricVehicle::~ElectricVehicle()
{

}