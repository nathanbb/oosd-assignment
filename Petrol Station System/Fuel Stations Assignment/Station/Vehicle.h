#pragma once
//#include "FuelPump.h"
using namespace std;

//-----------------------------------------------------------------------------
// Name: Vehicle
// Desc: Class used to store information about a vehicle using the system
//-----------------------------------------------------------------------------
class Vehicle
{
public:
	Vehicle() { };						// Empty constructor, initializes the class
	Vehicle(string, bool);				// Constructor, initializes the class
	~Vehicle();							// Destructor, frees memory used by the class

	string		GetNumberPlate() {return numberPlate;}	// Returns the vehicles num plate
	bool		Loyal() {return hasLoyaltyCard;}		// Returns whether the owner has a loyalty card
	
private:
	void		SetFuelRequired();		// Sets the fuel the car requries from the station

	string		numberPlate;			// Stores the number plate of the vehicle

	bool		hasLoyaltyCard;			// Stores whether the owner has a loyalty card
};

//-----------------------------------------------------------------------------
// Name: PetrolDieselVehicle
// Desc: Class used to store information about a Petrol or Diesel vehicle
// using the system.
//-----------------------------------------------------------------------------
class PetrolDieselVehicle : public Vehicle
{
public:
	PetrolDieselVehicle() : Vehicle() { };			// Empty constructor, initializes the class
	PetrolDieselVehicle(string, bool, float);		// Constructor, initializes the class and the superclass
	~PetrolDieselVehicle();							// Destructor, frees memory used by the class

	float	GetRequiredFuel() {return fuelRequired;}// Returns the fuel required

private:
	float	fuelRequired;							// Stores the amount of fuel required
};

//-----------------------------------------------------------------------------
// Name: ElectricVehicle
// Desc: Class used to store information about an electric vehicle 
// using the system.
//-----------------------------------------------------------------------------
class ElectricVehicle : public Vehicle
{
public:
	ElectricVehicle() : Vehicle() { };				// Empty constructor, initializes the class
	ElectricVehicle(string, bool, int);				// Constructor, initializes the class and the superclass
	~ElectricVehicle();								// Destructor, frees memory used by the class
	
	int	GetRequiredFuel() {return batteryPercentageRequired;}	// Returns the charge required

private:
	int		batteryPercentageRequired;				// Stores the amount of charge required
};